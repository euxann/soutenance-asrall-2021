@echo off
cls
color 4F
:question
ECHO *** CMFG - SCRIPT D'INSTALLATIONS DE LOGICIELS ***
ECHO(
ECHO(
ECHO 0: Installation de Chocolatey
ECHO(
ECHO 1: Script CCAA - Avec Office 2007 
ECHO(
ECHO 2: Script CCAA - Sans Office 2007
ECHO( 
ECHO 3: Script Commun Avec Office 2007
ECHO(
ECHO 4: Script Commun Sans Office 2007
ECHO(
ECHO 5: Script HAD Avec Office 2007
ECHO(
ECHO 6: Script HAD sans Office 2007 
ECHO(
ECHO 7: Script SESSAD avec Office 2007 
ECHO(
ECHO 8: Script SESSAD sans Office 2007
ECHO( 
ECHO 9: Script SSIAD avec Office 2007 
ECHO(
ECHO 10: Script SSIAD sans Office 2007 : 
ECHO(
ECHO(

set /p choix= Choisissez une action :   

if /I "%choix%"=="0" (goto :Z)
if /I "%choix%"=="1" (goto :A)
if /I "%choix%"=="2" (goto :B)
if /I "%choix%"=="3" (goto :C)
if /I "%choix%"=="4" (goto :D)
if /I "%choix%"=="5" (goto :E)
if /I "%choix%"=="6" (goto :F)
if /I "%choix%"=="7" (goto :G)
if /I "%choix%"=="8" (goto :H)
if /I "%choix%"=="9" (goto :I)
if /I "%choix%"=="10" (goto :J)
goto question

:A
echo.

choco install -y firefox vlc adobereader anydesk openvpn classic-shell teamviewer
choco install -y googlechrome --ignore-checksums
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Tightvnc\tightvnc-2.7.10-setup-64bit.msi
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Xnview\XnView-win-full.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\ESET\Eset_version_8.exe
timeout /t 360
START \\Ahss.local\Donnees\partage\INFORMATIQUE\Logiciels\Office2007\SETUP.exe
timeout /t 360
START \\Ahss.local\Donnees\partage\INFORMATIQUE\Logiciels\Office2007\office2007sp3-kb2526086-fullfile-fr-fr.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\Lotus_notes853_basic_Win_FR.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\lotus_notes853FP6_basic_win.exe
xcopy \\Ahss.local\donnees\partage\INFORMATIQUE\raccourciCCAA C:\Users\Public\Desktop

goto end

:B
echo.

choco install -y firefox vlc adobereader anydesk openvpn classic-shell teamviewer
choco install -y googlechrome --ignore-checksums
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Tightvnc\tightvnc-2.7.10-setup-64bit.msi
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Xnview\XnView-win-full.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\ESET\Eset_version_8.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\Lotus_notes853_basic_Win_FR.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\lotus_notes853FP6_basic_win.exe
xcopy \\Ahss.local\donnees\partage\INFORMATIQUE\raccourciCCAA C:\Users\Public\Desktop

goto end

:C
echo.

choco install -y firefox vlc adobereader anydesk openvpn classic-shell teamviewer
choco install -y googlechrome --ignore-checksums
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Tightvnc\tightvnc-2.7.10-setup-64bit.msi
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Xnview\XnView-win-full.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\ESET\Eset_version_8.exe
timeout /t 360
START \\Ahss.local\Donnees\partage\INFORMATIQUE\Logiciels\Office2007\SETUP.exe
timeout /t 360
START \\Ahss.local\Donnees\partage\INFORMATIQUE\Logiciels\Office2007\office2007sp3-kb2526086-fullfile-fr-fr.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\Lotus_notes853_basic_Win_FR.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\lotus_notes853FP6_basic_win.exe

goto end

:D
echo.

choco install -y firefox vlc adobereader anydesk openvpn classic-shell teamviewer
choco install -y googlechrome --ignore-checksums
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Tightvnc\tightvnc-2.7.10-setup-64bit.msi
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Xnview\XnView-win-full.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\ESET\Eset_version_8.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\Lotus_notes853_basic_Win_FR.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\lotus_notes853FP6_basic_win.exe

goto end

:E
echo.

choco install -y firefox vlc adobereader anydesk openvpn classic-shell teamviewer
choco install -y googlechrome --ignore-checksums
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Tightvnc\tightvnc-2.7.10-setup-64bit.msi
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Xnview\XnView-win-full.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\ESET\Eset_version_8.exe
timeout /t 360
START \\Ahss.local\Donnees\partage\INFORMATIQUE\Logiciels\Office2007\SETUP.exe
timeout /t 360
START \\Ahss.local\Donnees\partage\INFORMATIQUE\Logiciels\Office2007\office2007sp3-kb2526086-fullfile-fr-fr.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\Lotus_notes853_basic_Win_FR.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\lotus_notes853FP6_basic_win.exe
timeout /t 360
xcopy \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Anthadine\raccourci\ C:\Users\Public\Desktop 

goto end

:F
echo.

choco install -y firefox vlc adobereader anydesk openvpn classic-shell teamviewer
choco install -y googlechrome --ignore-checksums
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Tightvnc\tightvnc-2.7.10-setup-64bit.msi
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Xnview\XnView-win-full.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\ESET\Eset_version_8.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\Lotus_notes853_basic_Win_FR.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\lotus_notes853FP6_basic_win.exe
timeout /t 360
xcopy \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\BASE-PROD\ C:\Users\Public\Desktop  

goto end

:G
echo.

choco install -y firefox vlc adobereader anydesk openvpn classic-shell teamviewer
choco install -y googlechrome --ignore-checksums
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Tightvnc\tightvnc-2.7.10-setup-64bit.msi
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Xnview\XnView-win-full.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\ESET\Eset_version_8.exe
timeout /t 360
START \\Ahss.local\Donnees\partage\INFORMATIQUE\Logiciels\Office2007\SETUP.exe
timeout /t 360
START \\Ahss.local\Donnees\partage\INFORMATIQUE\Logiciels\Office2007\office2007sp3-kb2526086-fullfile-fr-fr.exe
timeout /t 360
START \\Ahss.local\Donnees\partage\INFORMATIQUE\Logiciels\Office2007\SETUP.exe
timeout /t 360
START \\Ahss.local\Donnees\partage\INFORMATIQUE\Logiciels\Office2007\office2007sp3-kb2526086-fullfile-fr-fr.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\Lotus_notes853_basic_Win_FR.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\lotus_notes853FP6_basic_win.exe
xcopy \\Ahss.local\donnees\partage\INFORMATIQUE\raccourci_sessad C:\Users\Public\Desktop

goto end

:H
echo.

choco install -y firefox vlc adobereader anydesk openvpn classic-shell teamviewer
choco install -y googlechrome --ignore-checksums
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Tightvnc\tightvnc-2.7.10-setup-64bit.msi
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Xnview\XnView-win-full.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\ESET\Eset_version_8.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\Lotus_notes853_basic_Win_FR.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\lotus_notes853FP6_basic_win.exe
xcopy \\Ahss.local\donnees\partage\INFORMATIQUE\raccourci_sessad C:\Users\Public\Desktop

goto end

:I
echo.

choco install -y firefox vlc adobereader anydesk openvpn classic-shell teamviewer
choco install -y googlechrome --ignore-checksums
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Tightvnc\tightvnc-2.7.10-setup-64bit.msi
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Xnview\XnView-win-full.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\ESET\Eset_version_8.exe
timeout /t 360
START \\Ahss.local\Donnees\partage\INFORMATIQUE\Logiciels\Office2007\SETUP.exe
timeout /t 360
START \\Ahss.local\Donnees\partage\INFORMATIQUE\Logiciels\Office2007\office2007sp3-kb2526086-fullfile-fr-fr.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\Lotus_notes853_basic_Win_FR.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\lotus_notes853FP6_basic_win.exe
timeout /t 360
xcopy \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\BASE-PROD\ C:\Users\Public\Desktop 

goto end

:J
echo.

choco install -y firefox vlc adobereader anydesk openvpn classic-shell teamviewer
choco install -y googlechrome --ignore-checksums
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Tightvnc\tightvnc-2.7.10-setup-64bit.msi
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Xnview\XnView-win-full.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\ESET\Eset_version_8.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\Lotus_notes853_basic_Win_FR.exe
timeout /t 360
START \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\Lotus\LOTUS_853\lotus_notes853FP6_basic_win.exe
timeout /t 360
xcopy \\Ahss.local\donnees\partage\INFORMATIQUE\Logiciels\BASE-PROD\ C:\Users\Public\Desktop 

goto end

:Z
echo.

powershell Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

goto end

:end
echo.
Echo Appuyez sur une touche pour quitter
pause > nul